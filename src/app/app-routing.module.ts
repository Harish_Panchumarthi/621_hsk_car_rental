import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AboutUsComponent} from './pages/about-us/about-us.component';
import {LocationsComponent} from './pages/locations/locations.component';
import {BookCarComponent} from './pages/book-car/book-car.component';

const routes: Routes = [
  {
    path: 'about-us',
    component: AboutUsComponent
  },
  {
    path: 'locations',
    component: LocationsComponent
  },
  {
    path: 'book-car',
    component: BookCarComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
